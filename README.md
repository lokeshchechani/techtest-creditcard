# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Tech Test : Sample Credit Card Rest Api
* 0.0.1-SNAPSHOT


### How do I get set up? ###

* Perquisites

     Maven :  [Apache Maven](https://maven.apache.org/guides/index.html)
     
     Java 8 :  [Java SDK](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

* Build (Complile with UnitTest)
        
        mvn clean install

* Run/Deploy
        
        mvn spring-boot:run

* Access the api
        
        Api Docs (Swagger)
        ------------------
            http://localhost:8080/v2/api-docs
        
        Hosted Swagger UI
        ------------------
            http://localhost:8080/swagger-ui.html
        
        
        Sample Get Request (Should return preloaded warm up data)
        ----------------
            http://localhost:8080/api/creditcards
        

##### Reference 
For further reference, please consider the following sections:

*  [Spring Boot](https://spring.io/projects/spring-boot)


### Who do I talk to? ###

* Lokesh Chechani [contact](mailto:lokesh.chechani@gmail.com)
