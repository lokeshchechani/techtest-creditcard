package com.techtest.cc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * @author lokesh.chechani
 *
 * Rest Controller intergration test
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CredirCardControllerTest {

    private String apiPath = "/api/creditcards";
    private String postRequestValid = "{\"cardNo\" : \"8763\",\"name\" :\"cc1\",\"limit\" : \"£100\"}";
    private String postRequestInvalid = "{\"cardNo\" : \"1111\",\"name\" :\"cc2\",\"limit\" : \"£100\"}";
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void whenCardPost_thenSuccessResponse() throws Exception {
        this.mockMvc.perform(post(apiPath)
                .content(postRequestValid)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());

    }

    @Test
    public void whenCardPost_thenFailedResponse_BadRequest() throws Exception {
        this.mockMvc.perform(post(apiPath)
                .content(postRequestInvalid)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasItem("Invalid credit card number (Luhn check failed), Supply valid card number")));

    }
}
