package com.techtest.cc.data;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;


/**
 * @author lokesh.chechani
 *
 * Jpa Repository integration test
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class CreditCardRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private CreditCardRepository cardRepository;

    @Test
    public void whenFind_thenReturnCard() {
        // given
        CreditCard newCard = new CreditCard("8862","preloaded-cc1","£1000",100);
        entityManager.persist(newCard);
        entityManager.flush();

        // when
        CreditCard found = cardRepository.findByCardNo("8862");

        // then
        Assert.assertEquals(found.getName(),newCard.getName());
    }

    @Test(expected = ConstraintViolationException.class)
    public void whenSave_thenReturnLuhnCheckFailed() {
        // given
        CreditCard newCard = new CreditCard("1111","preloaded-cc_invalidluhn","£1000",100);

        // when
        entityManager.persist(newCard);
        entityManager.flush();

        // then exception as expected

    }


}
