package com.techtest.cc.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.techtest.cc.constraints.ValidLuhn;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;

/**
 * @author lokesh.chechani
 *
 * Entity to hold credit card information
 */

@Data
@RequiredArgsConstructor
@NoArgsConstructor
@Entity
public class CreditCard {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NonNull //For Lombok to generate RequiredArgs constructor.
    @NotEmpty(message = "cardNo must not be empty")
    @ValidLuhn(message = "Invalid credit card number (Luhn check failed), Supply valid card number")
    private String cardNo;

    @NonNull
    @NotEmpty(message = "name must not be empty")
    private String name;

    @NonNull
    @NotEmpty(message = "limit must not be empty")
    @JsonProperty("limit") // h2 database has limit as reserved word so JPA <-> DDL creation failed.
    private String cardLimit;

    @NonNull
    private long balance;

}
