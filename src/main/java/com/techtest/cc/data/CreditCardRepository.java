package com.techtest.cc.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * @author lokesh.chechani
 *
 * Spring data repository for Credit card database basic crud operation.
 */
@Repository
public interface CreditCardRepository extends JpaRepository<CreditCard, Long> {

    CreditCard findByCardNo(String cardNo);

}
