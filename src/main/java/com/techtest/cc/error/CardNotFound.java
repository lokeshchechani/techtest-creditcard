package com.techtest.cc.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author lokesh.chechani
 *
 * Card not found rest excpetion
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class CardNotFound extends RuntimeException {

    public CardNotFound(Long id) {
        super("Credit card id not found : " + id);
    }

}
