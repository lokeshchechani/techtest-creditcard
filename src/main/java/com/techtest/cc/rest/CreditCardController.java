package com.techtest.cc.rest;

import com.techtest.cc.data.CreditCard;
import com.techtest.cc.data.CreditCardRepository;
import com.techtest.cc.error.CardNotFound;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author lokesh.chechani
 *
 * Rest Controller to expose api for Credit Card resource
 */
@RestController
@RequestMapping("api/creditcards")
@Slf4j
public class CreditCardController {

    @Autowired
    private CreditCardRepository cardRepository;

    //FindAll
    @GetMapping
    List<CreditCard> getAll(){
        return cardRepository.findAll();
    }

    //Save
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    CreditCard create(@RequestBody @Valid CreditCard card){
        log.debug("Executing post request : {}", card.getName());
        //Making balance default to 0 on post
        card.setBalance(0);
        return cardRepository.save(card);
    }

    //FindOne
    @GetMapping("/{id}")
    CreditCard findOne(@PathVariable Long id){
        log.debug("Finding card with id : {}", id);
        return cardRepository.findById(id)
                .orElseThrow(() -> new CardNotFound(id));
    }

    @PutMapping("/{id}")
    CreditCard update(@PathVariable Long id, @RequestBody @Valid CreditCard newCard){
        log.debug("Updating card with id : {}", id);
        return cardRepository.findById(id)
                .map(card -> {
                    card.setName(newCard.getName());
                    card.setBalance(newCard.getBalance());
                    card.setCardLimit(newCard.getCardLimit());
                    card.setCardNo(newCard.getCardNo());
                    return cardRepository.save(card);
                })
                .orElseGet(() -> {
                    newCard.setId(id);
                    return cardRepository.save(newCard);
                });
    }

    @DeleteMapping("/{id}")
    void deleteCard(@PathVariable Long id) {
        log.debug("Deleting card with id : {}", id);
        cardRepository.deleteById(id);
    }

}
