package com.techtest.cc.constraints;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author lokesh.chechani
 *
 * LuhnValidator - Java code to validate a number against the Luhn algorithm
 * Validator class to do validation for @ValidLuhn annotation
 *
 *
 * @Reference : http://de.wikipedia.org/wiki/Luhn-Algorithmus
 *          Checks whether a string of digits is a valid credit card number according to the Luhn algorithm. 1. Starting with the second to last digit and
 *           moving left, double the value of all the alternating digits. For any digits that thus become 10 or more, add their digits together. For example,
 *           1111 becomes 2121, while 8763 becomes 7733 (from (1+6)7(1+2)3). 2. Add all these digits together. For example, 1111 becomes 2121, then 2+1+2+1 is
 *           6; while 8763 becomes 7733, then 7+7+3+3 is 20. 3. If the total ends in 0 (put another way, if the total modulus 10 is 0), then the number is valid
 *           according to the Luhn formula, else it is not valid. So, 1111 is not valid (as shown above, it comes out to 6), while 8763 is valid (as shown
 *           above, it comes out to 20).
 *
 *
 */
@Slf4j
public class LuhnValidator implements ConstraintValidator<ValidLuhn, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context)
    {
        log.debug("Luhn validation for supplied creditcard");
        if(StringUtils.isEmpty(value)){ //Empty value
            return false;
        }
        int[] digits = value.trim().chars().toArray();
        int sum = 0;
        int length = digits.length;
        for (int i = 0; i < length; i++)
        {
            // get digits in reverse order
            int digit = digits[length - i - 1];
            // every 2nd number multiply with 2
            if (i % 2 == 1)
                digit *= 2;
            sum += digit > 9 ? digit - 9 : digit;
        }
        return sum % 10 == 0;
    }
}
