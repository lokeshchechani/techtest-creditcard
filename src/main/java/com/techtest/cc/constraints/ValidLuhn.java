package com.techtest.cc.constraints;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author lokesh.chechani
 *
 * Custom annotation for Luhn Check
 */
@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = LuhnValidator.class)
@Documented
public @interface ValidLuhn {

    String message() default "Luhn check failed, supply valid creditcard number";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
