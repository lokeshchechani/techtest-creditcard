package com.techtest.cc.constraints;

import lombok.NonNull;

import javax.validation.constraints.NotEmpty;
import java.lang.annotation.*;

/**
 * @author lokesh.chechani
 * To encapsulate both @NotNull(JPA one) and @NonNull(lombok one) into single annotation
 */

@Deprecated //Come back again to find solution for two conflicting RetentionPolicy
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.LOCAL_VARIABLE, ElementType.TYPE_USE})
@Retention(RetentionPolicy.CLASS)
@NotEmpty
@NonNull
@Inherited
@Documented
public @interface NuN {
}

