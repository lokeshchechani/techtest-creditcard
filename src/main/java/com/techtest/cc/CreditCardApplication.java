package com.techtest.cc;

import com.techtest.cc.data.CreditCard;
import com.techtest.cc.data.CreditCardRepository;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class CreditCardApplication {

    @Autowired
    CreditCardRepository creditCardRepository;

    public static void main(String[] args) {
        SpringApplication.run(CreditCardApplication.class, args);
    }

    @Bean
    InitializingBean preLoadData(){
        return () -> {
            creditCardRepository.save(new CreditCard("8763","preloaded-cc1","£1000",100));
           // creditCardRepository.save(new CreditCard("8862","preloaded-cc2","£2000",-100));
        };
    }

}
